package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.INotifyService;
import ru.renessans.jvschool.volkov.tm.dto.Command;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandTypeException;
import ru.renessans.jvschool.volkov.tm.exception.incorrect.IncorrectCommandRegistrationException;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public final class NotifyService implements INotifyService {

    private final ICommandService commandService;

    public NotifyService(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public String getTerminalCommandNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();
        return notifyByType(CommandType.CMD, command);
    }

    @Override
    public String getArgumentCommandNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();
        return notifyByType(CommandType.ARG, command);
    }

    @Override
    public List<Command> addNotifyCommonCommands() {
        final AtomicReference<String> commonNotify = new AtomicReference<>("");
        final CommandType type = CommandType.COMMON;

        final List<Command> commons = this.commandService.getCommandsByType(type);
        if (ValidRuleUtil.isNullOrEmpty(commons))
            throw new IncorrectCommandRegistrationException(type.getTitle());

        commons.forEach(command -> {
            commonNotify.updateAndGet(v -> v + command.toString() + "\n");
            Command.HELP.setNotification(commonNotify.get());
        });
        return commons;
    }

    @Override
    public List<Command> addNotifyTerminalCommands() {
        final AtomicReference<String> argumentNotify = new AtomicReference<>("");
        final CommandType type = CommandType.CMD;

        final List<Command> commands = this.commandService.getCommandsByType(type);
        if (ValidRuleUtil.isNullOrEmpty(commands))
            throw new IncorrectCommandRegistrationException(type.getTitle());

        commands.forEach(command -> {
            argumentNotify.updateAndGet(v -> v + command.getCommand() + "\n");
            Command.COMMAND.setNotification(argumentNotify.get());
        });
        return commands;
    }

    @Override
    public List<Command> addNotifyArgumentCommands() {
        final AtomicReference<String> commandNotify = new AtomicReference<>("");
        final CommandType type = CommandType.ARG;

        final List<Command> arguments = this.commandService.getCommandsByType(type);
        if (ValidRuleUtil.isNullOrEmpty(arguments))
            throw new IncorrectCommandRegistrationException(type.getTitle());

        arguments.forEach(command -> {
            commandNotify.updateAndGet(v -> v + command.getArgument() + "\n");
            Command.ARGUMENT.setNotification(commandNotify.get());
        });
        return arguments;
    }

    private String notifyByType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) throw new EmptyCommandTypeException();
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();

        String notify = "";
        if (commandType.isCommon()) {
            notify = this.commandService.getNotifyByType(CommandType.COMMON, command);
        } else if (commandType.isArgument()) {
            notify = this.commandService.getNotifyByType(CommandType.ARG, command);
        } else if (commandType.isCommand()) {
            notify = this.commandService.getNotifyByType(CommandType.CMD, command);
        }
        if (ValidRuleUtil.isNullOrEmpty(notify)) throw new UnknownCommandException(command);
        return notify;
    }

}