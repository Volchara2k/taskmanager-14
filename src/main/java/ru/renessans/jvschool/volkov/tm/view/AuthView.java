package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.IAuthView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessDeniedException;
import ru.renessans.jvschool.volkov.tm.exception.security.LogOutFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.Objects;

public final class AuthView implements IAuthView {

    @Override
    public String getLine() {
        System.out.println(NotifyConst.ADD_DATA_MSG);
        return ScannerUtil.nextLine();
    }

    @Override
    public void print(final AuthState state) {
        if (Objects.isNull(state)) {
            System.out.println(NotifyConst.FAIL_RESULT_MSG);
            throw new AccessDeniedException();
        }
        if (!state.isSuccess()) {
            System.out.println(NotifyConst.FAIL_RESULT_MSG);
            throw new AccessDeniedException(state.getTitle());
        }
        System.out.println(NotifyConst.SUCCESS_RESULT_MSG);
    }

    @Override
    public void print(final User user) {
        if (Objects.isNull(user)) {
            System.out.println(NotifyConst.FAIL_RESULT_MSG);
            throw new EmptyProjectException();
        }
        System.out.printf("\n%s\nИдентификатор: %s.\n%s",
                user, user.getId(), NotifyConst.SUCCESS_RESULT_MSG);
    }

    @Override
    public void print(final boolean logout) {
        if (!logout) {
            System.out.println(NotifyConst.FAIL_RESULT_MSG);
            throw new LogOutFailureException();
        }
        System.out.println(NotifyConst.SUCCESS_RESULT_MSG);
    }

}