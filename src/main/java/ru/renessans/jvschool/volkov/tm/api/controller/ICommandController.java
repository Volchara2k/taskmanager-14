package ru.renessans.jvschool.volkov.tm.api.controller;

public interface ICommandController {

    void registrationCommandTypes();

    void registrationCommands();

}