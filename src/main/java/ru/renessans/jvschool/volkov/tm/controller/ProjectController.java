package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.List;

public final class ProjectController implements ICrudController {

    private final ICrudService<Project> projectService;

    private final ICrudView<Project> projectView;

    private final IAuthService authService;

    public ProjectController(final ICrudService<Project> projectService,
                             final IAuthService authService,
                             final ICrudView<Project> projectView

    ) {
        this.projectService = projectService;
        this.authService = authService;
        this.projectView = projectView;
    }

    @Override
    public void list() {
        final String userId = this.authService.getUserId();
        final List<Project> projects = this.projectService.getAll(userId);
        this.projectView.print(projects);
    }

    @Override
    public void create() {
        final String userId = this.authService.getUserId();
        final String title = this.projectView.getLine();
        final String description = this.projectView.getLine();
        final Project project = this.projectService.add(userId, title, description);
        this.projectView.print(project);
    }

    @Override
    public void updateByIndex() {
        final String userId = this.authService.getUserId();
        final Integer index = ScannerUtil.nextInteger() - 1;
        final String title = this.projectView.getLine();
        final String description = this.projectView.getLine();
        final Project project = this.projectService.updateByIndex(userId, index, title, description);
        this.projectView.print(project);
    }

    @Override
    public void updateById() {
        final String userId = this.authService.getUserId();
        final String id = this.projectView.getLine();
        final String title = this.projectView.getLine();
        final String description = this.projectView.getLine();
        final Project project = this.projectService.updateById(userId, id, title, description);
        this.projectView.print(project);
    }

    @Override
    public void removeByIndex() {
        final String userId = this.authService.getUserId();
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Project project = this.projectService.removeByIndex(userId, index);
        this.projectView.print(project);
    }

    @Override
    public void removeById() {
        final String userId = this.authService.getUserId();
        final String id = this.projectView.getLine();
        final Project project = this.projectService.removeById(userId, id);
        this.projectView.print(project);
    }

    @Override
    public void removeByTitle() {
        final String userId = this.authService.getUserId();
        final String title = this.projectView.getLine();
        final Project project = this.projectService.removeByTitle(userId, title);
        this.projectView.print(project);
    }

    @Override
    public void removeAll() {
        final String userId = this.authService.getUserId();
        final List<Project> projects = this.projectService.removeAll(userId);
        this.projectView.print(projects);
    }

    @Override
    public void viewByIndex() {
        final String userId = this.authService.getUserId();
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Project project = this.projectService.getByIndex(userId, index);
        this.projectView.print(project);
    }

    @Override
    public void viewById() {
        final String userId = this.authService.getUserId();
        final String id = this.projectView.getLine();
        final Project project = this.projectService.getById(userId, id);
        this.projectView.print(project);
    }

    @Override
    public void viewByTitle() {
        final String userId = this.authService.getUserId();
        final String title = this.projectView.getLine();
        final Project project = this.projectService.getByTitle(userId, title);
        this.projectView.print(project);
    }

}