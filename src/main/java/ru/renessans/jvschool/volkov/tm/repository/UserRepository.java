package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        this.users.add(user);
        return user;
    }

    @Override
    public User getById(final String id) {
        for (User user : this.users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User getByLogin(final String login) {
        for (User user : this.users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        return this.users;
    }

    @Override
    public User removeById(final String id) {
        final User user = getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Override
    public User removeByUser(final User user) {
        this.users.remove(user);
        return user;
    }

}