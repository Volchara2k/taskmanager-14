package ru.renessans.jvschool.volkov.tm.api.view;

import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthView {

    String getLine();

    void print(AuthState state);

    void print(User user);

    void print(boolean logout);

}