package ru.renessans.jvschool.volkov.tm.util;

import java.util.Collection;

public interface ValidRuleUtil {

    static boolean isNullOrEmpty(final String string) {
        return string == null || string.isEmpty();
    }

    static boolean isNullOrEmpty(final Integer integer) {
        return integer == null || integer < 0;
    }

    static boolean isNullOrEmpty(final String... strings) {
        return strings == null || strings.length < 1;
    }

    static boolean isNullOrEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

}