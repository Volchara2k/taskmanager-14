package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.IAuthController;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.view.IAuthView;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.model.User;

public final class AuthController implements IAuthController {

    private final IAuthService authService;

    private final IAuthView authView;

    public AuthController(final IAuthService authService,
                          final IAuthView authView
    ) {
        this.authService = authService;
        this.authView = authView;
    }

    @Override
    public void signIn() {
        final String login = this.authView.getLine();
        final String password = this.authView.getLine();
        final AuthState authState = this.authService.signIn(login, password);
        this.authView.print(authState);
    }

    @Override
    public void signUp() {
        final String login = this.authView.getLine();
        final String password = this.authView.getLine();
        final User user = this.authService.signUp(login, password);
        this.authView.print(user);
    }

    @Override
    public void logOut() {
        final boolean logout = this.authService.logOut();
        this.authView.print(logout);
    }

}