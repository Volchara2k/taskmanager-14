package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyPasswordException extends AbstractRuntimeException {

    private static final String EMPTY_PASSWORD = "Ошибка! Парамерт \"пароль\" является пустым или null!\n";

    public EmptyPasswordException() {
        super(EMPTY_PASSWORD);
    }

}