package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.dto.Command;

import java.util.List;

public interface ICommandService {

    List<CommandType> addCommandTypes();

    List<Command> addCommonCommands();

    List<Command> addTerminalCommands();

    List<Command> addArgumentCommands();

    String getNotifyByType(CommandType commandType, String command);

    List<Command> getCommandsByType(CommandType commandType);

}