package ru.renessans.jvschool.volkov.tm.api.service;

import java.util.List;

public interface ICrudService<T> {

    T add(String userId, String title, String description);

    T updateByIndex(String userId, Integer index, String title, String description);

    T updateById(String userId, String id, String title, String description);

    T removeByIndex(String userId, Integer index);

    T removeById(String userId, String id);

    T removeByTitle(String userId, String title);

    List<T> removeAll(String userId);

    T getByIndex(String userId, Integer index);

    T getById(String userId, String id);

    T getByTitle(String userId, String title);

    List<T> getAll(String userId);

}