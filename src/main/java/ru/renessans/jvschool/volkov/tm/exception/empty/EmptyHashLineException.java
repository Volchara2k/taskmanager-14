package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyHashLineException extends AbstractRuntimeException {

    private static final String EMPTY_EMAIL = "Ошибка! Парамерт \"строка для хеширования\" является пустым или null!\n";

    public EmptyHashLineException() {
        super(EMPTY_EMAIL);
    }

}