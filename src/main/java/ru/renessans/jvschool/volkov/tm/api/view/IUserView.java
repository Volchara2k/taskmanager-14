package ru.renessans.jvschool.volkov.tm.api.view;

import ru.renessans.jvschool.volkov.tm.model.User;

public interface IUserView {

    String getLine();

    void print(User user);

}