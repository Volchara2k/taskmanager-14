package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.IAuthRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessDeniedException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.HashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Objects;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private final IAuthRepository authRepository;

    public AuthService(final IUserService userService,
                       final IAuthRepository authRepository
    ) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Override
    public String getUserId() {
        if (Objects.isNull(this.authRepository.getUserId())) throw new AccessDeniedException();
        return this.authRepository.getUserId();
    }

    @Override
    public AuthState signIn(final String login, final String password) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        final User user = this.userService.getByLogin(login);
        if (Objects.isNull(user)) return AuthState.USER_NOT_FOUND;
        final String passwordHash = HashUtil.getInstance().saltHashLine(password);
        if (ValidRuleUtil.isNullOrEmpty(passwordHash)) return AuthState.APPLICATION_ERROR;
        if (!passwordHash.equals(user.getPasswordHash())) return AuthState.INVALID_PASSWORD;

        this.authRepository.signUp(user);
        return AuthState.SUCCESS;
    }

    @Override
    public User signUp(final String login, final String password) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        return this.userService.add(login, password);
    }

    @Override
    public User signUp(final String login, final String password, final String email) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(email)) throw new EmptyEmailException();

        return this.userService.add(login, password, email);
    }

    @Override
    public User signUp(final String login, final String password, final UserRole role) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();

        return this.userService.add(login, password, role);
    }

    @Override
    public boolean logOut() {
        return this.authRepository.logOut();
    }

}