package ru.renessans.jvschool.volkov.tm.api.repository;

import java.util.List;

public interface ICrudRepository<T> {

    T add(String userId, T object);

    T removeByIndex(String userId, Integer index);

    T removeById(String userId, String id);

    T removeByTitle(String userId, String title);

    T remove(String userId, T object);

    List<T> removeAll(String userId);

    T getByIndex(String userId, Integer index);

    T getById(String userId, String id);

    T getByTitle(String userId, String title);

    List<T> getAll(String userId);

}