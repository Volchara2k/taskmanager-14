package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public final class TaskRepository implements ICrudRepository<Task> {

    private final List<Task> tasks = new CopyOnWriteArrayList<>();

    @Override
    public Task add(final String userId, final Task task) {
        task.setUserId(userId);
        this.tasks.add(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @Override
    public Task removeByTitle(final String userId, final String title) {
        final Task task = getByTitle(userId, title);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @Override
    public Task remove(final String userId, final Task task) {
        this.tasks.remove(task);
        return task;
    }

    @Override
    public List<Task> removeAll(final String userId) {
        final List<Task> userTasks = new ArrayList<>();
        for (Task task : this.tasks) {
            if (userId.equals(task.getUserId())) {
                userTasks.add(task);
                this.tasks.remove(task);
            }
        }
        return userTasks;
    }

    @Override
    public Task getByIndex(final String userId, final Integer index) {
        final List<Task> userTasks = new ArrayList<>();
        for (Task task : this.tasks) {
            if (userId.equals(task.getUserId())) userTasks.add(task);
        }
        return userTasks.get(index);
    }

    @Override
    public Task getById(final String userId, final String id) {
        for (final Task task : this.tasks) {
            if (userId.equals(task.getUserId()) &&
                    id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task getByTitle(final String userId, final String title) {
        for (final Task task : this.tasks) {
            if (userId.equals(task.getUserId()) &&
                    title.equals(task.getTitle())) return task;
        }
        return null;
    }

    @Override
    public List<Task> getAll(final String userId) {
        final List<Task> userTasks = new ArrayList<>();
        for (Task task : this.tasks) {
            if (userId.equals(task.getUserId())) userTasks.add(task);
        }
        return userTasks;
    }

}