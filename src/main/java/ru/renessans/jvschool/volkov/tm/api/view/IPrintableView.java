package ru.renessans.jvschool.volkov.tm.api.view;

public interface IPrintableView {

    void print(String printable);

}