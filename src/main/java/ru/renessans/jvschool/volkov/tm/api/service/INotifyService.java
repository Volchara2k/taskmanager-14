package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.dto.Command;

import java.util.List;

public interface INotifyService {

    String getTerminalCommandNotify(final String command);

    String getArgumentCommandNotify(final String command);

    List<Command> addNotifyCommonCommands();

    List<Command> addNotifyTerminalCommands();

    List<Command> addNotifyArgumentCommands();

}