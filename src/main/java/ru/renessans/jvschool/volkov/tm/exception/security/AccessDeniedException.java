package ru.renessans.jvschool.volkov.tm.exception.security;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class AccessDeniedException extends AbstractRuntimeException {

    private static final String ACCESS_DENIED = "Ошибка! Доступ запрещён!\n";

    private static final String ACCESS_DENIED_FORMAT = "Ошибка! %s Доступ запрещён!\n";

    public AccessDeniedException() {
        super(ACCESS_DENIED);
    }

    public AccessDeniedException(final String message) {
        super(String.format(ACCESS_DENIED_FORMAT, message));
    }

}