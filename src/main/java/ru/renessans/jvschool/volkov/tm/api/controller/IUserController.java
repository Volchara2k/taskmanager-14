package ru.renessans.jvschool.volkov.tm.api.controller;

public interface IUserController {

    void updatePassword();

    void viewProfile();

    void editProfile();

}