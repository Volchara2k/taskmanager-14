package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandTypeException;
import ru.renessans.jvschool.volkov.tm.dto.Command;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<CommandType> addCommandTypes() {
        return this.commandRepository.addCommandTypes(
                Arrays.asList(
                        CommandType.COMMON, CommandType.CMD, CommandType.ARG
                )
        );
    }

    @Override
    public List<Command> addCommonCommands() {
        return this.commandRepository.addCommands(
                CommandType.COMMON,
                Arrays.asList(
                        Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND,
                        Command.PROJECT_CREATE, Command.PROJECT_LIST, Command.PROJECT_CLEAR, Command.PROJECT_UPDATE_BY_INDEX,
                        Command.PROJECT_UPDATE_BY_ID, Command.PROJECT_DELETE_BY_INDEX, Command.PROJECT_DELETE_BY_ID,
                        Command.PROJECT_DELETE_BY_TITLE, Command.PROJECT_VIEW_BY_INDEX, Command.PROJECT_VIEW_BY_ID,
                        Command.PROJECT_VIEW_BY_TITLE, Command.TASK_CREATE, Command.TASK_LIST, Command.TASK_CLEAR,
                        Command.TASK_UPDATE_BY_INDEX, Command.TASK_UPDATE_BY_ID, Command.TASK_DELETE_BY_INDEX,
                        Command.TASK_DELETE_BY_ID, Command.TASK_DELETE_BY_TITLE, Command.TASK_VIEW_BY_INDEX,
                        Command.TASK_VIEW_BY_ID, Command.TASK_VIEW_BY_TITLE, Command.SIGN_IN, Command.SIGN_UP,
                        Command.LOG_OUT, Command.PASSWORD_UPDATE, Command.VIEW_PROFILE, Command.EDIT_PROFILE, Command.EXIT
                )
        );
    }

    @Override
    public List<Command> addTerminalCommands() {
        return this.commandRepository.addCommands(
                CommandType.CMD,
                Arrays.asList(
                        Command.HELP, Command.VERSION, Command.ABOUT, Command.INFO, Command.ARGUMENT, Command.COMMAND,
                        Command.PROJECT_CREATE, Command.PROJECT_LIST, Command.PROJECT_CLEAR, Command.PROJECT_UPDATE_BY_INDEX,
                        Command.PROJECT_UPDATE_BY_ID, Command.PROJECT_DELETE_BY_INDEX, Command.PROJECT_DELETE_BY_ID,
                        Command.PROJECT_DELETE_BY_TITLE, Command.PROJECT_VIEW_BY_INDEX, Command.PROJECT_VIEW_BY_ID,
                        Command.PROJECT_VIEW_BY_TITLE, Command.TASK_CREATE, Command.TASK_LIST, Command.TASK_CLEAR,
                        Command.TASK_UPDATE_BY_INDEX, Command.TASK_UPDATE_BY_ID, Command.TASK_DELETE_BY_INDEX,
                        Command.TASK_DELETE_BY_ID, Command.TASK_DELETE_BY_TITLE, Command.TASK_VIEW_BY_INDEX,
                        Command.TASK_VIEW_BY_ID, Command.TASK_VIEW_BY_TITLE, Command.SIGN_IN, Command.SIGN_UP,
                        Command.LOG_OUT, Command.PASSWORD_UPDATE, Command.VIEW_PROFILE, Command.EDIT_PROFILE, Command.EXIT
                )
        );
    }

    @Override
    public List<Command> addArgumentCommands() {
        return this.commandRepository.addCommands(
                CommandType.ARG,
                Arrays.asList(
                        Command.HELP, Command.VERSION, Command.ABOUT,
                        Command.INFO, Command.ARGUMENT, Command.COMMAND
                )
        );
    }

    @Override
    public String getNotifyByType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) throw new EmptyCommandTypeException();
        if (ValidRuleUtil.isNullOrEmpty(command)) throw new EmptyCommandException();
        return this.commandRepository.getNotifyByType(commandType, command);
    }

    @Override
    public List<Command> getCommandsByType(final CommandType commandType) {
        if (Objects.isNull(commandType)) throw new EmptyCommandTypeException();
        return commandRepository.getCommandsByType(commandType);
    }

}