package ru.renessans.jvschool.volkov.tm.enumeration;

public enum UserRole {

    ADMIN("Администратор"),

    USER("Пользователь");

    private final String title;

    UserRole(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

}