package ru.renessans.jvschool.volkov.tm.exception.security;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public class LogOutFailureException extends AbstractRuntimeException {

    private static final String LOG_OUT_FAILURE = "Ошибка! Выход из системы завершился неожиданным результатом!\n";

    public LogOutFailureException() {
        super(LOG_OUT_FAILURE);
    }

}