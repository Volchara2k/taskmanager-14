package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyUserIdException extends AbstractRuntimeException {

    private static final String EMPTY_USER_ID =
            "Ошибка! Парамерт \"идентификатор пользователя\" является пустым или null!\n";

    public EmptyUserIdException() {
        super(EMPTY_USER_ID);
    }

}