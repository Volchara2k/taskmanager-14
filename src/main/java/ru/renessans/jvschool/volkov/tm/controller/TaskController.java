package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICrudController;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.ICrudService;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.List;

public final class TaskController implements ICrudController {

    private final ICrudService<Task> taskService;

    private final ICrudView<Task> taskView;

    private final IAuthService authService;

    public TaskController(final ICrudService<Task> taskService,
                          final IAuthService authService,
                          final ICrudView<Task> taskView
    ) {
        this.taskService = taskService;
        this.authService = authService;
        this.taskView = taskView;
    }

    @Override
    public void create() {
        final String userId = this.authService.getUserId();
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task task = this.taskService.add(userId, title, description);
        this.taskView.print(task);
    }

    @Override
    public void list() {
        final String userId = this.authService.getUserId();
        final List<Task> tasks = this.taskService.getAll(userId);
        this.taskView.print(tasks);
    }

    @Override
    public void updateByIndex() {
        final String userId = this.authService.getUserId();
        final Integer index = ScannerUtil.nextInteger() - 1;
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task updatedTask = this.taskService.updateByIndex(userId, index, title, description);
        this.taskView.print(updatedTask);
    }

    @Override
    public void updateById() {
        final String userId = this.authService.getUserId();
        final String id = this.taskView.getLine();
        final String title = this.taskView.getLine();
        final String description = this.taskView.getLine();
        final Task updatedTask = this.taskService.updateById(userId, id, title, description);
        this.taskView.print(updatedTask);
    }

    @Override
    public void removeByIndex() {
        final String userId = this.authService.getUserId();
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Task task = this.taskService.removeByIndex(userId, index);
        this.taskView.print(task);
    }

    @Override
    public void removeById() {
        final String userId = this.authService.getUserId();
        final String id = this.taskView.getLine();
        final Task task = this.taskService.removeById(userId, id);
        this.taskView.print(task);
    }

    @Override
    public void removeByTitle() {
        final String userId = this.authService.getUserId();
        final String title = this.taskView.getLine();
        final Task task = this.taskService.removeByTitle(userId, title);
        this.taskView.print(task);
    }

    @Override
    public void removeAll() {
        final String userId = this.authService.getUserId();
        final List<Task> tasks = this.taskService.removeAll(userId);
        this.taskView.print(tasks);
    }

    @Override
    public void viewByIndex() {
        final String userId = this.authService.getUserId();
        final Integer index = ScannerUtil.nextInteger() - 1;
        final Task task = this.taskService.getByIndex(userId, index);
        this.taskView.print(task);
    }

    @Override
    public void viewById() {
        final String userId = this.authService.getUserId();
        final String id = this.taskView.getLine();
        final Task task = this.taskService.getById(userId, id);
        this.taskView.print(task);
    }

    @Override
    public void viewByTitle() {
        final String userId = this.authService.getUserId();
        final String title = this.taskView.getLine();
        final Task project = this.taskService.getByTitle(userId, title);
        this.taskView.print(project);
    }

}