package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.IPrintableView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

public final class PrintableView implements IPrintableView {

    @Override
    public void print(final String printable) {
        if (ValidRuleUtil.isNullOrEmpty(printable)) {
            System.out.println(NotifyConst.FAIL_RESULT_MSG);
            return;
        }
        System.out.println(printable);
    }

}