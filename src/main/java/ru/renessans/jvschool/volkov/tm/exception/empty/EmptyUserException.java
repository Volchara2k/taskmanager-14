package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyUserException extends AbstractRuntimeException {

    private static final String EMPTY_USER =
            "Ошибка! Парамерт \"пользователь\" является null!\n";

    public EmptyUserException() {
        super(EMPTY_USER);
    }

}