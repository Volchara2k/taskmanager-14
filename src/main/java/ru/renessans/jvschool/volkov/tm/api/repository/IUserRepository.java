package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    User getById(String id);

    User getByLogin(String login);

    List<User> getAll();

    User removeById(String id);

    User removeByLogin(String login);

    User removeByUser(User user);

}