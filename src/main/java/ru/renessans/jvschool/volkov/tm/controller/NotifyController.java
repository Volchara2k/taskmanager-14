package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.INotifyController;
import ru.renessans.jvschool.volkov.tm.api.service.INotifyService;
import ru.renessans.jvschool.volkov.tm.api.view.IPrintableView;
import ru.renessans.jvschool.volkov.tm.dto.Command;

import java.util.List;

public final class NotifyController implements INotifyController {

    private final INotifyService notifyService;

    private final IPrintableView notifyView;

    private static final String FORMAT_REGISTRATION_MSG = "%s успешно зарегистрированы!\n";

    public NotifyController(final INotifyService notifyService,
                            final IPrintableView notifyView
    ) {
        this.notifyService = notifyService;
        this.notifyView = notifyView;
    }

    @Override
    public void registrationNotifyCommands() {
        final List<Command> commons = this.notifyService.addNotifyCommonCommands();
        printCommands(commons, "Уведомления для общих команд");
        final List<Command> commands = this.notifyService.addNotifyTerminalCommands();
        printCommands(commands, "Уведомления для терминальных команд");
        final List<Command> arguments = this.notifyService.addNotifyArgumentCommands();
        printCommands(arguments, "Уведомления для программных аргументов");
    }

    @Override
    public void printCommandNotify(final String command) {
        final String notify = this.notifyService.getTerminalCommandNotify(command);
        this.notifyView.print(notify);
    }

    @Override
    public void printArgumentNotify(final String command) {
        final String notify = this.notifyService.getArgumentCommandNotify(command);
        this.notifyView.print(notify);
    }

    private void printCommands(final List<Command> commands, final String format) {
        commands.forEach(command -> this.notifyView.print(command.name()));
        this.notifyView.print(String.format(FORMAT_REGISTRATION_MSG, format));
    }

}