package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyLoginException extends AbstractRuntimeException {

    private static final String EMPTY_LOGIN = "Ошибка! Парамерт \"логин\" является пустым или null!\n";

    public EmptyLoginException() {
        super(EMPTY_LOGIN);
    }

}