package ru.renessans.jvschool.volkov.tm.view;

import ru.renessans.jvschool.volkov.tm.api.view.IUserView;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.Objects;

public final class UserView implements IUserView {

    @Override
    public String getLine() {
        System.out.println(NotifyConst.ADD_DATA_MSG);
        return ScannerUtil.nextLine();
    }

    @Override
    public void print(final User user) {
        if (Objects.isNull(user)) {
            System.out.println(NotifyConst.FAIL_RESULT_MSG);
            throw new EmptyProjectException();
        }
        System.out.printf("\n%s\nИдентификатор: %s.\n%s",
                user, user.getId(), NotifyConst.SUCCESS_RESULT_MSG);
    }

}