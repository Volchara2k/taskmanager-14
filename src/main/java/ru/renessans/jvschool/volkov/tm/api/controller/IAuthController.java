package ru.renessans.jvschool.volkov.tm.api.controller;

public interface IAuthController {

    void signIn();

    void signUp();

    void logOut();

}