package ru.renessans.jvschool.volkov.tm.bootstrap;

import ru.renessans.jvschool.volkov.tm.api.controller.*;
import ru.renessans.jvschool.volkov.tm.api.repository.IAuthRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.*;
import ru.renessans.jvschool.volkov.tm.api.view.IAuthView;
import ru.renessans.jvschool.volkov.tm.api.view.IPrintableView;
import ru.renessans.jvschool.volkov.tm.api.view.ICrudView;
import ru.renessans.jvschool.volkov.tm.api.view.IUserView;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.controller.*;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.repository.*;
import ru.renessans.jvschool.volkov.tm.service.*;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.view.*;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(userService, authRepository);

    private final IAuthView authView = new AuthView();

    private final IAuthController authController = new AuthController(authService, authView);

    private final IUserView userView = new UserView();

    private final IUserController userController = new UserController(userService, authService, userView);


    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IPrintableView commandView = new PrintableView();

    private final INotifyService notifyService = new NotifyService(commandService);

    private final INotifyController notifyController = new NotifyController(notifyService, commandView);


    private final ICrudRepository<Task> taskRepository = new TaskRepository();

    private final ICrudService<Task> taskService = new TaskService(taskRepository);

    private final ICrudView<Task> taskView = new TaskView();

    private final ICrudController taskController = new TaskController(taskService, authService, taskView);


    private final ICrudRepository<Project> projectRepository = new ProjectRepository();

    private final ICrudService<Project> projectService = new ProjectService(projectRepository);

    private final ICrudView<Project> projectView = new ProjectView();

    private final ICrudController projectController = new ProjectController(projectService, authService, projectView);

    public Bootstrap() {
        final ICommandController commandController = new CommandController(commandService, commandView);
        commandController.registrationCommandTypes();
        commandController.registrationCommands();
        this.notifyController.registrationNotifyCommands();
    }

    public void run(final String... args) {
        initUsers();
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(args);
    }

    private void initUsers() {
        this.userService.add("1", "1");
        this.userService.add("2", "2", UserRole.ADMIN);
    }

    private void terminalCommandPrintLoop() {
        String command = "";
        while (!CmdConst.EXIT.equals(command)) {
            command = ScannerUtil.nextLine();
            try {
                this.notifyController.printCommandNotify(command);
                executeTaskOrProject(command);
            } catch (final Exception e) {
                System.err.print(e.getMessage());
            }
        }
    }

    private void argumentPrint(final String... args) {
        final String arg = args[0];
        try {
            this.notifyController.printArgumentNotify(arg);
        } catch (final Exception e) {
            System.err.print(e.getMessage());
        }
    }

    private void executeTaskOrProject(final String factor) {
        switch (factor) {
            case CmdConst.TASK_CREATE:
                this.taskController.create();
                break;
            case CmdConst.TASK_LIST:
                this.taskController.list();
                break;
            case CmdConst.TASK_CLEAR:
                this.taskController.removeAll();
                break;
            case CmdConst.PROJECT_CREATE:
                this.projectController.create();
                break;
            case CmdConst.PROJECT_LIST:
                this.projectController.list();
                break;
            case CmdConst.PROJECT_CLEAR:
                this.projectController.removeAll();
                break;
            case CmdConst.TASK_UPDATE_BY_INDEX:
                this.taskController.updateByIndex();
                break;
            case CmdConst.TASK_UPDATE_BY_ID:
                this.taskController.updateById();
                break;
            case CmdConst.TASK_DELETE_BY_INDEX:
                this.taskController.removeByIndex();
                break;
            case CmdConst.TASK_DELETE_BY_ID:
                this.taskController.removeById();
                break;
            case CmdConst.TASK_DELETE_BY_TITLE:
                this.taskController.removeByTitle();
                break;
            case CmdConst.TASK_VIEW_BY_INDEX:
                this.taskController.viewByIndex();
                break;
            case CmdConst.TASK_VIEW_BY_ID:
                this.taskController.viewById();
                break;
            case CmdConst.TASK_VIEW_BY_TITLE:
                this.taskController.viewByTitle();
                break;
            case CmdConst.PROJECT_UPDATE_BY_INDEX:
                this.projectController.updateByIndex();
                break;
            case CmdConst.PROJECT_UPDATE_BY_ID:
                this.projectController.updateById();
                break;
            case CmdConst.PROJECT_DELETE_BY_INDEX:
                this.projectController.removeByIndex();
                break;
            case CmdConst.PROJECT_DELETE_BY_ID:
                this.projectController.removeById();
                break;
            case CmdConst.PROJECT_DELETE_BY_TITLE:
                this.projectController.removeByTitle();
                break;
            case CmdConst.PROJECT_VIEW_BY_INDEX:
                this.projectController.viewByIndex();
                break;
            case CmdConst.PROJECT_VIEW_BY_ID:
                this.projectController.viewById();
                break;
            case CmdConst.PROJECT_VIEW_BY_TITLE:
                this.projectController.viewByTitle();
                break;
            case CmdConst.SIGN_IN:
                this.authController.signIn();
                break;
            case CmdConst.SIGN_UP:
                this.authController.signUp();
                break;
            case CmdConst.LOG_OUT:
                this.authController.logOut();
                break;
            case CmdConst.UPDATE_PASSWORD:
                this.userController.updatePassword();
                break;
            case CmdConst.VIEW_PROFILE:
                this.userController.viewProfile();
                break;
            case CmdConst.EDIT_PROFILE:
                this.userController.editProfile();
                break;
            default:
                break;
        }
    }

}