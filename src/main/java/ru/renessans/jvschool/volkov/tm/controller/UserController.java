package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.IUserController;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.api.view.IUserView;
import ru.renessans.jvschool.volkov.tm.model.User;

public final class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    private final IUserView userView;

    public UserController(final IUserService userService,
                          final IAuthService authService,
                          final IUserView userView
    ) {
        this.userService = userService;
        this.authService = authService;
        this.userView = userView;
    }

    @Override
    public void updatePassword() {
        final String userId = this.authService.getUserId();
        final String password = this.userView.getLine();
        final User user = this.userService.updatePasswordById(userId, password);
        this.userView.print(user);
    }

    @Override
    public void viewProfile() {
        final String userId = this.authService.getUserId();
        final User user = this.userService.getById(userId);
        this.userView.print(user);
    }

    @Override
    public void editProfile() {
        final String userId = this.authService.getUserId();
        final String firstName = this.userView.getLine();
        final User user = this.userService.editProfileById(userId, firstName);
        this.userView.print(user);
    }

}