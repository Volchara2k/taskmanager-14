package ru.renessans.jvschool.volkov.tm.api.controller;

public interface INotifyController {

    void registrationNotifyCommands();

    void printCommandNotify(String command);

    void printArgumentNotify(String command);

}