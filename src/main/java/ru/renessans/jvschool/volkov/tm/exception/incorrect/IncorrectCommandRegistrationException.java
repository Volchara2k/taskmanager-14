package ru.renessans.jvschool.volkov.tm.exception.incorrect;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public class IncorrectCommandRegistrationException extends AbstractRuntimeException {

    private static final String INCORRECT_COMMAND_REGISTRATION =
            "Ошибка! Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    public IncorrectCommandRegistrationException(final String message) {
        super(String.format(INCORRECT_COMMAND_REGISTRATION, message));
    }

}